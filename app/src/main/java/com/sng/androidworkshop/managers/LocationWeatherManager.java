package com.sng.androidworkshop.managers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;


import static android.support.v4.content.PermissionChecker.checkSelfPermission;

/**
 * Created by khanhnguyen on 5/20/17.
 */

public class LocationWeatherManager {
    private static final LocationWeatherManager ourInstance = new LocationWeatherManager();

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int CAMERA_REQUEST = INITIAL_REQUEST + 1;
    private static final int CONTACTS_REQUEST = INITIAL_REQUEST + 2;
    private static final int LOCATION_REQUEST = INITIAL_REQUEST + 3;

    public static LocationWeatherManager getInstance() {
        return ourInstance;
    }

    private LocationWeatherManager() {

    }

    public void requestLocationPermission(Context context) {
        ActivityCompat.requestPermissions((Activity) context, LOCATION_PERMS, LOCATION_REQUEST);
    }

    private boolean canAccessLocation(Context context) {
        return(hasPermission(context, Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessCamera(Context context) {
        return(hasPermission(context, Manifest.permission.CAMERA));
    }

    private boolean canAccessContacts(Context context) {
        return(hasPermission(context, Manifest.permission.READ_CONTACTS));
    }

    private boolean hasPermission(Context context, String perm) {
        return(PackageManager.PERMISSION_GRANTED == checkSelfPermission(context, perm));
    }
}
