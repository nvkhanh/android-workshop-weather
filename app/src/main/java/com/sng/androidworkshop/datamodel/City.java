package com.sng.androidworkshop.datamodel;

/**
 * Created by khanhnguyen on 5/3/17.
 */

public class City {
    public String cityName;
    public String cityCode;
    public String latitude;
    public String longitude;

    public City(String cityName) {
        this.cityName = cityName;
    }
}
