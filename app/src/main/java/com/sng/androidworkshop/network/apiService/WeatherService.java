package com.sng.androidworkshop.network.apiService;

import com.sng.androidworkshop.schema.response.CurrentWeatherResponse;
import com.sng.androidworkshop.schema.response.ForcastWeatherResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nguye on 4/26/2017.
 */

public interface WeatherService {
    @GET("current.json")
    Call<CurrentWeatherResponse> getCurrentWeather(@Query("key") String key, @Query("q") String q);

    @GET("forecast.json")
    Call<ForcastWeatherResponse> getForecast(@Query("key") String key, @Query("q") String q, @Query("days") String days);
}
