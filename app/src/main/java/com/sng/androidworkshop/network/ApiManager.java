package com.sng.androidworkshop.network;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.sng.androidworkshop.R;
import com.sng.androidworkshop.application.BaseApplication;
import com.sng.androidworkshop.network.apiService.WeatherService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nguye on 4/26/2017.
 */

public class ApiManager {
    private static ApiManager sApiManager;
    private static Retrofit sRetrofit;
    private static WeatherService sWeatherService;

    private ApiManager() {
        if (sRetrofit == null) {
            sRetrofit = createRetrofit();
        }
    }

    public static ApiManager getInstance() {
        if (sApiManager == null) {
            sApiManager = new ApiManager();
        }
        return sApiManager;
    }

    private Retrofit createRetrofit() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(String.format(BaseApplication.getContext().getString(R.string.global_base_url)))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public WeatherService getWeatherService(){
        if (sWeatherService == null){
            sWeatherService = sRetrofit.create(WeatherService.class);
        }
        return sWeatherService;
    }
}
