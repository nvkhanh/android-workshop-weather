package com.sng.androidworkshop.feature.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sng.androidworkshop.R;
import com.sng.androidworkshop.Utils.FragmentUtils;
import com.sng.androidworkshop.adapter.CityListAdapter;
import com.sng.androidworkshop.datamodel.City;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CityListFragment extends Fragment {

    boolean isBookMark = false;
    List<City> cityList;
    private RecyclerView recyclerView;
    private CityListAdapter mAdapter;

    public CityListFragment(boolean isBookMark) {
        this.isBookMark = isBookMark;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        cityList = new ArrayList<>();

        City city = new City("Hà Nội");
        city.cityCode = "Ha Noi";
        cityList.add(city);

        city = new City("Hồ Chí Minh");
        city.cityCode = "Ho Chi Minh";
        cityList.add(city);

        city = new City("Cần Thơ");
        city.cityCode = "Can Tho";
        cityList.add(city);

        View view = inflater.inflate(R.layout.fragment_city_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.cityList);

        mAdapter = new CityListAdapter(cityList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnClickItemListener(new CityListAdapter.OnClickItemListener() {
            @Override
            public void onClickCityItem(View cityView, City city) {
                DetailFragment detailFragment = new DetailFragment();
                detailFragment.locationName = city.cityName;
                FragmentUtils.replace(getFragmentManager(), detailFragment);
            }
        });

        return view;
    }

}
