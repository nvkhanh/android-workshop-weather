package com.sng.androidworkshop.feature.activities;

import android.Manifest;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.sng.androidworkshop.R;
import com.sng.androidworkshop.Utils.FragmentUtils;
import com.sng.androidworkshop.baseComponent.BaseActivity;
import com.sng.androidworkshop.feature.fragments.CityListFragment;
import com.sng.androidworkshop.feature.fragments.LocationFragment;
import com.sng.androidworkshop.managers.*;
import com.sng.androidworkshop.feature.weather.WeatherDetailActivity;

/**
 * Created by khanhnguyen on 4/26/2017.
 */

public class HomeActivity extends BaseActivity {

    private static int IndexCity = 0;
    private static int IndexBookMark = 1;
    private static int IndexLocation = 2;
    private CityListFragment cities = null;

    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    public  void onBackPressed() {
        if (this.cities != null) {
            FragmentUtils.replace(getSupportFragmentManager(), this.cities);
        }

    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.cities =  new CityListFragment(false);
        FragmentUtils.replace(getSupportFragmentManager(), this.cities);
        //new DrawerBuilder().withActivity(this).build();
        createSideMenu();
        LocationWeatherManager.getInstance().requestLocationPermission(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

//    private void createSideMenu() {
//        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.side_menu_my_location);
//        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withIdentifier(2).withName(R.string.side_menu_my_bookmark);
//
////create the drawer and remember the `Drawer` result object
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        Drawer result = new DrawerBuilder()
//                .withActivity(this)
//                .withToolbar(toolbar)
//                .addDrawerItems(
//                        item1,
//                        new DividerDrawerItem(),
//                        item2,
//                        new SecondaryDrawerItem().withName(R.string.side_menu_my_bookmark)
//                )
//                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
//                    @Override
//                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
//                        return true;
//                    }
//                })
//                .build();
//
//    }

    private void createSideMenu() {
        ListView mDrawerListView;

        final DrawerLayout mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Replace the default action bar with a Toolbar so the navigation drawer appears above it
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        // These lines are needed to display the top-left hamburger button
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Make the hamburger button work
        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawer,R.string.app_name,R.string.app_name){
            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }
        };
        mDrawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        // Change the TextView message when ListView item is clicked
        mDrawerListView = (ListView) findViewById(R.id.left_drawer);
        mDrawerListView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDrawer.closeDrawer(GravityCompat.START);

                if (position == IndexCity) { //Citi list
                    FragmentUtils.replace(getSupportFragmentManager(), new CityListFragment(false));
                } else if (position == IndexBookMark) {
                    FragmentUtils.replace(getSupportFragmentManager(), new CityListFragment(true));
                } else if (position == IndexLocation) {
                    FragmentUtils.replace(getSupportFragmentManager(), new LocationFragment());
                }


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...
        return super.onOptionsItemSelected(item);
    }
}
