package com.sng.androidworkshop.feature.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sng.androidworkshop.R;
import com.sng.androidworkshop.Utils.FragmentUtils;
import com.sng.androidworkshop.adapter.CityListAdapter;
import com.sng.androidworkshop.adapter.ForecastAdapter;
import com.sng.androidworkshop.datamodel.City;
import com.sng.androidworkshop.network.ApiManager;
import com.sng.androidworkshop.schema.response.CurrentWeatherResponse;
import com.sng.androidworkshop.schema.response.ForcastWeatherResponse;
import com.sng.androidworkshop.schema.response.Forecastday;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sng.androidworkshop.application.BaseApplication.getContext;

/**
 * Created by khanhnguyen on 5/12/17.
 */

public class DetailFragment extends Fragment {

    List<City> cityList;

    public String locationName = "";

    @Bind(R.id.recycle_forecast)
    public RecyclerView recyclerView;

    @Bind(R.id.tvAddress)
    public TextView tvAddress;

    @Bind(R.id.tvCelsius)
    public TextView tvCelsius;

    @Bind(R.id.tvStatus)
    public TextView tvStatus;

    @Bind(R.id.tvWind)
    public TextView tvWind;

    @Bind(R.id.tvHumidity)
    public TextView tvHumidity;

    @Bind(R.id.imgCurrent)
    public ImageView imgCurrent;

    private ForecastAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_weather_detail, container, false);
        ButterKnife.bind(this, view);

        mAdapter = new ForecastAdapter();
        mAdapter.setOnClickItemListener(new ForecastAdapter.OnClickItemListener() {
            @Override
            public void onClickView(View cityView, Forecastday day) {

            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }



    @Override
    public void onResume() {
        super.onResume();
        this.fetchForecast();
        this.fetchCurrentWeather();
    }

    private void fetchCurrentWeather() {
        ApiManager.getInstance().getWeatherService().getCurrentWeather(getString(R.string.weather_api_key),locationName).enqueue(new Callback<CurrentWeatherResponse>() {
            @Override
            public void onResponse(Call<CurrentWeatherResponse> call, Response<CurrentWeatherResponse> response) {
                if (response == null || response.body() == null){
                    return;
                }

                if (response.body().getCurrent() != null){
                    tvCelsius.setText(String.valueOf(response.body().getCurrent().getTempC()) + "\u2103");
                    tvStatus.setText(String.valueOf(response.body().getCurrent().getCondition().getText()));
                    tvHumidity.setText("Humidity: " + String.valueOf(response.body().getCurrent().getHumidity() + "%"));
                    tvWind.setText("Wind: " + String.valueOf(response.body().getCurrent().getWindKph() + " km/h"));
                    String imgURL = String.valueOf(response.body().getCurrent().getCondition().getIcon());
                    Picasso.with(getContext()).load("http:" + imgURL).into(imgCurrent);
                }

                if (response.body().getLocation() != null){
                    tvAddress.setText(response.body().getLocation().getName());
                }
            }

            @Override
            public void onFailure(Call<CurrentWeatherResponse> call, Throwable t) {
                Log.d("sng", "api call fail");
            }
        });
    }

    private void fetchForecast() {
        ApiManager.getInstance().getWeatherService().getForecast(getString(R.string.weather_api_key),locationName,"4").enqueue(new Callback<ForcastWeatherResponse>() {
            @Override
            public void onResponse(Call<ForcastWeatherResponse> call, Response<ForcastWeatherResponse> response) {
                if (response == null || response.body() == null){
                    return;
                }

                if (response.body().getForecast() != null) {
                    List<Forecastday> days = response.body().getForecast().getForecastday();

                    mAdapter.setDaylist(days);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ForcastWeatherResponse> call, Throwable t) {
                Log.d("sng", "api call fail");
            }
        });
    }
}
