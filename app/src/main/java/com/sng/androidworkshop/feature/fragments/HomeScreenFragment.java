package com.sng.androidworkshop.feature.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sng.androidworkshop.R;
import com.sng.androidworkshop.baseComponent.BaseFragment;
import com.sng.androidworkshop.network.ApiManager;
import com.sng.androidworkshop.schema.response.CurrentWeatherResponse;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by khanhnguyen on 4/26/2017.
 */

public class HomeScreenFragment extends BaseFragment {
    @Bind(R.id.imgBanner)
    ImageView mImgBanner;
    @Bind(R.id.tvCityName)
    TextView mTvCityName;
    @Bind(R.id.rlBanner)
    RelativeLayout mRlBanner;
    @Bind(R.id.tvTempa)
    TextView mTvTempa;
    @Bind(R.id.tvWind)
    TextView mTvWind;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        fetchData();
        loadImageBanner();
        return view;
    }

    private void loadImageBanner() {
        Picasso.with(getContext()).load("http://2sao.vietnamnetjsc.vn//2016/06/07/15/30/hot-girl-lao-nguoi-goc-viet-vua-giau-vua-xinh-vua-gioi_19.jpg").into(mImgBanner);
    }

    private void fetchData() {
        ApiManager.getInstance().getWeatherService().getCurrentWeather(getString(R.string.weather_api_key),"Paris").enqueue(new Callback<CurrentWeatherResponse>() {
            @Override
            public void onResponse(Call<CurrentWeatherResponse> call, Response<CurrentWeatherResponse> response) {
                if (response == null || response.body() == null){
                    return;
                }
                if (response.body().getCurrent() !=null){
                    mTvWind.setText(String.valueOf(response.body().getCurrent().getWindKph()));
                    mTvTempa.setText(String.valueOf(response.body().getCurrent().getTempC()));
                }
                if (response.body().getLocation()!=null){
                    mTvCityName.setText(response.body().getLocation().getName());
                }
            }

            @Override
            public void onFailure(Call<CurrentWeatherResponse> call, Throwable t) {
                Log.d("sng", "api call fail");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
