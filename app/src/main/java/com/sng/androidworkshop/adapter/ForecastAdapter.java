package com.sng.androidworkshop.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sng.androidworkshop.R;
import com.sng.androidworkshop.datamodel.City;
import com.sng.androidworkshop.schema.response.Forecastday;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;

import static com.sng.androidworkshop.application.BaseApplication.getContext;

/**
 * Created by khanhnguyen on 5/23/17.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.MyViewHolder> {



    private List<Forecastday> daylist;
    public ForecastAdapter.OnClickItemListener clickListener;

    public void setOnClickItemListener(ForecastAdapter.OnClickItemListener clickListener) {
        this.clickListener = clickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvCondition;

        public ImageView imgForecast;
        public TextView tvDate;

        public Forecastday forcastDay;

        public MyViewHolder(View view) {
            super(view);
            tvCondition = (TextView) view.findViewById(R.id.forecast_condition);
            imgForecast = (ImageView) view.findViewById(R.id.forecast_image);
            tvDate = (TextView) view.findViewById(R.id.forecast_date);
            view.getLayoutParams().height = 200;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                clickListener.onClickView(view, forcastDay);
            }
        }
    }

    public ForecastAdapter() {

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_forecast_day, parent, false);

        return new ForecastAdapter.MyViewHolder(itemView);
    }

    public ForecastAdapter(List<Forecastday> days) {
        this.daylist = days;
    }

    public void setDaylist(List<Forecastday> daylist) {
        this.daylist = daylist;
    }

    @Override
    public void onBindViewHolder(ForecastAdapter.MyViewHolder holder, int position) {
        Forecastday day = daylist.get(position);
        holder.forcastDay = day;
        holder.tvCondition.setText(day.getDay().getCondition().getText());
        holder.tvDate.setText(day.getDate());
        Picasso.with(getContext()).load(day.getDay().getCondition().getIcon()).into(holder.imgForecast);
    }

    @Override
    public int getItemCount() {
        if (daylist != null) {
            return daylist.size();
        }

        return 0;
    }

    public interface OnClickItemListener {
        public void onClickView(View cityView, Forecastday day) ;
    }
}
