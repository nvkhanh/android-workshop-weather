package com.sng.androidworkshop.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sng.androidworkshop.R;
import com.sng.androidworkshop.datamodel.City;

import java.util.List;

/**
 * Created by khanhnguyen on 5/3/17.
 */

public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.MyViewHolder> {

    private List<City> cityList;
    public OnClickItemListener cityDelegate;

    public void setOnClickItemListener(CityListAdapter.OnClickItemListener cityDelegate) {
        this.cityDelegate = cityDelegate;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title;
        public City city;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.cityTitle);
            view.getLayoutParams().height = 200;
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (cityDelegate != null) {
                cityDelegate.onClickCityItem(view, this.city);
            }
        }
    }


    public CityListAdapter(List<City> cities) {
        this.cityList = cities;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_city, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        City city = cityList.get(position);
        holder.city = city;
        holder.title.setText(city.cityName);
    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }

    public interface OnClickItemListener {
        public void onClickCityItem(View cityView, City city) ;
    }
}