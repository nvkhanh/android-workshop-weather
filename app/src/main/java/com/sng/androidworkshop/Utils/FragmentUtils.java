package com.sng.androidworkshop.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.sng.androidworkshop.R;
import com.sng.androidworkshop.feature.fragments.CityListFragment;

/**
 * Created by khanhnguyen on 5/12/17.
 */

public class FragmentUtils {
    public static void replace(FragmentManager manager, Fragment targetFragMent) {
        manager.beginTransaction().replace(R.id.fragmentContent, targetFragMent, targetFragMent.getClass().getSimpleName()).commitAllowingStateLoss();
    }
}
