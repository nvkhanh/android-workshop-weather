package com.sng.androidworkshop.application;

import android.app.Application;

/**
 * Created by khanhnguyen on 4/26/2017.
 */

public class BaseApplication extends Application {
    public static BaseApplication sSingleton;

    public static BaseApplication getContext() {
        return sSingleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sSingleton = this;
    }
}
